# Plantilla per implementar els casos pràctics

- Per cada cas pràctic s'ha de lliurar un fitxer en format markdown, amb el número d'ordre, guió baix (\_) i el nom del cas.
P.e.: `6_zoos.md`

- L'esquema conceptual (entitat-relació) s'ha de fer amb [draw.io](https://www.draw.io/)). Haureu de generar un enllaç públic i l'esquema exportat a format png.

- El contingut del fitxer ha de seguir la següent estructura numerada de sota. `Esborreu d'aquí (inclòs) cap a dalt quan feu l'entrega`

# 1. Títol

Propter hoc causa vestri database vis ad effectum deducendi. Lorem ipsum dolor sit amet consectetur adipiscing elit, lectus quam porta feugiat placerat per mollis, fringilla fermentum laoreet aliquet posuere tempus. Odio mauris felis sagittis fusce mattis cubilia turpis vestibulum, est iaculis vehicula sociosqu venenatis sapien auctor, risus sodales ultricies curabitur habitant vitae porttitor. Congue morbi sociosqu per lobortis sagittis volutpat erat maecenas eleifend, fusce dui convallis ligula neque mattis nascetur mi.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas xxx](http://...)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas xxx](./path/to/ER.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Relacio1(<ins>idXX</ins>, atribut1, atribut1, ...)  
  Relacio2(<ins>idZZ</ins>, atribut1, atribut2, ...)
  \...

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
x|x|x
x|x|x

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)
